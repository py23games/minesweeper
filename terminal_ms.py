import random
import re


class Board:

    def __init__(self, b_size, numb_bombs):
        self.b_size = b_size
        self.numb_bombs = numb_bombs
        self.board = self.make_new_board()
        self.board_values()
        self.dug = set()

    def make_new_board(self):
        board = [[None for _ in range(self.b_size)]for _ in range(self.b_size)]

        planted = 0
        while planted < self.numb_bombs:
            location = random.randint(0, self.b_size**2 - 1)
            row = location // self.b_size
            col = location % self.b_size

            if board[row][col] == '*':
                continue

            board[row][col] = '*'
            planted += 1

        return board

    def board_values(self):
        print("🐍 board_values ~ self.b_size", self.b_size)
        for r in range(self.b_size):
            for c in range(self.b_size):
                if self.board[r][c] == '*':
                    continue
                self.board[r][c] = self.get_num_neighbor_bombs(r, c)

    def get_num_neighbor_bombs(self, row, col):
        num_of_neighbor_bombs = 0

        for r in range(max(0, row-1), min(self.b_size-1, row+1)+1):
            for c in range(max(0, col-1), min(self.b_size-1, col+1)+1):
                if r == row and c == col:
                    continue
                if self.board[r][c] == '*':
                    num_of_neighbor_bombs += 1

        return num_of_neighbor_bombs

    def dig(self, row, col):
        self.dug.add((row, col))

        if self.board[row][col] == '*':
            return False
        elif self.board[row][col] > 0:
            return True

        for r in range(max(0, row-1), min(self.b_size-1, row+1)+1):
            for c in range(max(0, col-1), min(self.b_size-1, col+1)+1):
                if (r, c) in self.dug:
                    continue
                self.dig(r, c)

        return True

    def __str__(self):
        visible_board = [[None for _ in range(
            self.b_size)] for _ in range(self.b_size)]

        for row in range(self.b_size):
            for col in range(self.b_size):
                if (row, col) in self.dug:
                    visible_board[row][col] = str(self.board[row][col])
                else:
                    visible_board[row][col] = ' '

        # put this together in a string
        string_rep = ''
        # get max column widths for printing
        widths = []
        for idx in range(self.b_size):
            columns = map(lambda x: x[idx], visible_board)
            widths.append(len(max(columns, key=len)))

        # print the csv strings
        indices = [i for i in range(self.b_size)]
        indices_row = '   '
        cells = []
        for idx, col in enumerate(indices):
            format = '%-' + str(widths[idx]) + "s"
            cells.append(format % (col))
        indices_row += '  '.join(cells)
        indices_row += '  \n'

        for i in range(len(visible_board)):
            row = visible_board[i]
            string_rep += f'{i} |'
            cells = []
            for idx, col in enumerate(row):
                format = '%-' + str(widths[idx]) + "s"
                cells.append(format % (col))
            string_rep += ' |'.join(cells)
            string_rep += ' |\n'

        str_len = int(len(string_rep) / self.b_size)
        string_rep = indices_row + '-'*str_len + '\n' + string_rep + '-'*str_len

        return string_rep


def play(b_size=10, numb_bombs=10):
    board = Board(b_size, numb_bombs)
    safe = True

    while len(board.dug) < board.b_size ** 2 - numb_bombs:
        print(board)
        user_input = re.split(',(//s)*', input('Where dig? (row, col): '))
        row, col = int(user_input[0]), int(user_input[-1])
        print("🐍 File: minesweeper/terminal_ms.py | Line: 118 | play ~ row, col", row, col)

        if row < 0 or row >= board.b_size or col < 0 or col >= b_size:
            print('Invalid location: ' + (row, col))
            continue

        safe = board.dig(row, col)
        if not safe:
            break

    if safe:
        print('WOW! You won!')
    else:
        print('Sorry, try again... looser.')

        board.dug = [(r, c) for r in range(board.b_size)
                     for c in range(board.b_size)]
        print(board)


if __name__ == '__main__':
    play()
